# POC-architectural

## Architectural

- [architectural](https://gitlab.com/sodawafa/poc-architectural)

## Link code

- [Backend POC](https://gitlab.com/sodawafa/poc-spring)
- [FrontEnd POC](https://gitlab.com/sodawafa/application-web)


## Authors and acknowledgment
Author: Wafa Soda

Email address: sodawafa@gmail.com
